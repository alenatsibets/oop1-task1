import java.util.Scanner;

public class Test1 {
    private int number1;
    private int number2;

    public Test1() {
        this.number1 = 0;
        this.number2 = 0;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }
    public void display() {
        System.out.println("The first value is: " + this.getNumber1() + " The second value is: " + this.getNumber2());
    }
    public void change() {
        System.out.println("Enter two variables");
        Scanner scanner = new Scanner(System.in);
        int val1 = scanner.nextInt();
        int val2 = scanner.nextInt();
        this.setNumber1(val1);
        this.setNumber2(val2);
    }
    public int sum() {
        return this.getNumber1() + this.getNumber2();
    }

    public int theLargestValue() {
        if (this.getNumber1() > this.getNumber2()) {
            return this.getNumber1();
        } else if (this.getNumber2() > this.getNumber1()) {
            return this.getNumber2();
        } else {
            throw new RuntimeException("Values are equals");
        }

    }
}
