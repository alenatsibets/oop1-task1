public class Main {
    public static void main(String[] args) {
        Test1 a = new Test1();
        a.change();
        a.display();
        int sum = a.sum();
        System.out.println("The sum of two variables: " + sum);
        int max = a.theLargestValue();
        System.out.println("The largest of two variables: " + max);
    }

}